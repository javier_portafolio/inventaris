<?php

namespace App\Http\Controllers\Configuration;

use Validator;
use App\Categoria;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Enums\HttpStatus;

class CategoriaController extends Controller
{
    protected $respuesta = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show index.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index ()
    {
        return view('categoria.index');
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function listaCategorias (Request $request)
    {
    	try {
			$all = Categoria::where('eliminado', 0)->get();

			$this->respuesta["data"] = [];

			foreach ($all as $categoria) {

				$this->respuesta["data"][] = (object) [
					'id' => $categoria->id,
					'clase' => __($categoria->subclase()->clase()->clase),
					'subclase' => __($categoria->subclase()->sub_clase),
					'categoria' => __($categoria->categoria),
					'urlMostrar' => route("categoria.show", ['locale' => app()->getLocale(), 'categoria' => $categoria->id]),
					'urlEditar' => route("categoria.edit", ['locale' => app()->getLocale(), 'categoria' => $categoria->id]),
					'urlEliminar' => route("categoria.destroy", ['locale' => app()->getLocale(), 'categoria' => $categoria->id])
				];
			}

			if (empty($this->respuesta["data"])) {
				$httpStatus = HttpStatus::NOCONTENT;
			}
			else {
				$httpStatus = HttpStatus::OK;
			}
		} catch (\Exception $e) {
			$this->respuesta["mensaje"] = $e->getMessage()?? HttpStatus::ERROR();
			$httpStatus = HttpStatus::ERROR;
		}

		return response()->json($this->respuesta, $httpStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->respuesta["extras"] = (object) [
			"clases" => \App\Clase::where('eliminado', 0)->get(),
		];

		return response()->view('categoria.crear', $this->respuesta, HttpStatus::OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'clase' => ['required', 'numeric'],
            'subclase' => ['required', 'numeric'],
            'categoria' => ['required', 'regex:/^([a-zA-Z]+(.*))+$/'],
            'capacidad' => ['boolean'],
        ])->validate();

        $categoria = new Categoria;
        $categoria->sub_clase_id = $request->subclase;
        $categoria->categoria = $request->categoria;
        $categoria->ver_capacidad = $request->capacidad ?? false;

        try {
        	$categoria->save();

            /* ========== Register action on bitacora ========== */
            $bitacora = new \App\Bitacora();
            $modulo = \App\Modulo::where('modulo', 'categorias')->first();
            $accion = \App\Accion::where('accion', 'Create')->first();
            $descripcion = "Created Category";
            $bitacora->registro($modulo->id, $categoria->id, $accion->id, \Request::ip(), $descripcion);
            /* ================================================= */

            $httpStatus = HttpStatus::CREATED;
            $this->respuesta["mensaje"] = HttpStatus::CREATED();
        } catch (\Exception $e) {
        	$this->respuesta["mensaje"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        try {
            $categoria = Categoria::find($id);

            if (!empty($categoria)) {

                $this->respuesta["data"] = (object) [
                    "clase" => __($categoria->subclase()->clase()->clase),
                    "subclase" => __($categoria->subclase()->sub_clase),
                    "categoria" => __($categoria->categoria),
                    "ver_capacidad" => $categoria->ver_capacidad,
                ];

                return response()->view('categoria.mostrar', $this->respuesta, HttpStatus::OK);
            }
            else {
                $httpStatus = HttpStatus::NOCONTENT;
            }
        } catch (\Exception $e) {
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        try {
            $categoria = Categoria::find($id);

            if (!empty($categoria)) {

                $this->respuesta["data"] = (object) [
                    'id' => $categoria->id,
                    'clase' => $categoria->subclase()->clase()->id,
                    'subclase' => $categoria->sub_clase_id,
                    'categoria' => $categoria->categoria,
                    'capacidad' => $categoria->ver_capacidad
                ];

                $this->respuesta["extras"] = (object) [
                	'clases' => \App\Clase::where('eliminado', 0)->get(),
                	'subclases' => \App\Subclase::where('eliminado', 0)->where('clase_id', $categoria->subclase()->clase_id)->get(),
                ];

                return response()->view('categoria.editar', $this->respuesta, HttpStatus::OK);
            }
            else {
                $httpStatus = HttpStatus::NOCONTENT;
            }
        } catch (\Exception $e) {
            $this->respuesta["message"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        Validator::make($request->all(), [
            'clase' => ['required', 'numeric'],
            'subclase' => ['required', 'numeric'],
            'categoria' => ['required', 'regex:/^([a-zA-Z]+(.*))+$/'],
            'capacidad' => ['boolean'],
        ])->validate();

        $categoria = Categoria::find($id);
        $categoria->sub_clase_id = $request->subclase;
        $categoria->categoria = $request->categoria;
        $categoria->ver_capacidad = $request->capacidad ? true : false;

        try {
            if ($categoria->isDirty()) {
                $categoria->save();

                /* ========== Register action on bitacora ========== */
                $bitacora = new \App\Bitacora();
                $modulo = \App\Modulo::where('modulo', 'categorias')->first();
                $accion = \App\Accion::where('accion', 'Update')->first();
                $descripcion = "Updated Category";
                $bitacora->registro($modulo->id, $categoria->id, $accion->id, \Request::ip(), $descripcion);
                /* ================================================= */

                $httpStatus = HttpStatus::OK;
                $this->respuesta["mensaje"] = HttpStatus::OK();
            }
            else {
                $httpStatus = HttpStatus::NOCONTENT;
            }
        } catch (\Exception $e) {
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $categoria = Categoria::find($id);

        try {

            $categoria->eliminado = 1;

            $categoria->save();

            /* ========== Register action on bitacora ========== */
            $bitacora = new \App\Bitacora();
            $modulo = \App\Modulo::where('modulo', 'categorias')->first();
            $accion = \App\Accion::where('accion', 'Delete')->first();
            $descripcion = "Deleted Category";
            $bitacora->registro($modulo->id, $categoria->id, $accion->id, \Request::ip(), $descripcion);
            /* ================================================= */

            $httpStatus = HttpStatus::OK;
            $this->respuesta["mensaje"] = HttpStatus::OK();
        } catch (\Exception $e) {
            $httpStatus = HttpStatus::ERROR;
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
        }

        return response()->json($this->respuesta, $httpStatus);
    }

    /**
     * Get the categories of the subclass
     *
     * @param string $locale
     * @param int $subclase
     * @return \Illuminate\Http\Response
     */
    public function categorias ($locale, $subclase)
    {
        try {
            $resultado = Categoria::where('sub_clase_id', $subclase)->where('eliminado', 0)->get();

            if (empty($resultado)) {
                $httpStatus = HttpStatus::NOCONTENT;
            }
            else {
                $categorias = [];

                foreach ($resultado as $value) {
                    $aux = [
                        'id' => $value->id,
                        'categoria' => __($value->categoria),
                        'ver_capacidad' => $value->ver_capacidad
                    ];

                    array_push($categorias, $aux);
                }

                $httpStatus = HttpStatus::OK;
                $this->respuesta["categorias"] = $categorias;
            }
        } catch (\Exception $e) {
            $httpStatus = HttpStatus::ERROR;
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
        }

        return response()->json($this->respuesta, $httpStatus);
    }
}
