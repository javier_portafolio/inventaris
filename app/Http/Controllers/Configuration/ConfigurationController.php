<?php

namespace App\Http\Controllers\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show index.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index ()
    {
    	return view('configuration.index');
    }
}
