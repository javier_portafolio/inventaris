<?php

namespace App\Http\Controllers\Configuration;

use Validator;
use App\Estatu;
use App\Enums\HttpStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EstatuController extends Controller
{
	protected $respuesta = [];
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show index.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index ()
	{
		return view('estatu.index');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function estatus(Request $request)
	{
		try {
			$all = Estatu::where('eliminado', 0)->get();

			$this->respuesta["data"] = [];

			foreach ($all as $estatu) {

				$this->respuesta["data"][] = (object) [
					'id' => $estatu->id,
					'estado' => __($estatu->estado),
					'urlMostrar' => route("estatu.show", ['locale' => app()->getLocale(), 'estatu' => $estatu->id]),
					'urlEditar' => route("estatu.edit", ['locale' => app()->getLocale(), 'estatu' => $estatu->id]),
					'urlEliminar' => route("estatu.destroy", ['locale' => app()->getLocale(), 'estatu' => $estatu->id])
				];
			}

			if (empty($this->respuesta["data"])) {
				$httpStatus = HttpStatus::NOCONTENT;
			}
			else {
				$httpStatus = HttpStatus::OK;
			}
		} catch (\Exception $e) {
			$this->respuesta["mensaje"] = HttpStatus::ERROR();
			$httpStatus = HttpStatus::ERROR;
		}

		return response()->json($this->respuesta, $httpStatus);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return response()->view('estatu.crear', $this->respuesta, HttpStatus::OK);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		Validator::make($request->all(), [
            'estatu' => ['required', 'regex:/^([a-zA-Z]+(.*))+$/'],
        ])->validate();

        $estatu = new Estatu();
        $estatu->estado = $request->estatu;

        try {
            $estatu->save();

            /* ========== Register action on bitacora ========== */
            $bitacora = new \App\Bitacora();
            $modulo = \App\Modulo::where('modulo', 'estatus')->first();
            $accion = \App\Accion::where('accion', 'Create')->first();
            $descripcion = "Created Statu";
            $bitacora->registro($modulo->id, $estatu->id, $accion->id, \Request::ip(), $descripcion);
            /* ================================================= */

            $httpStatus = HttpStatus::CREATED;
            $this->respuesta["mensaje"] = HttpStatus::CREATED();
        } catch (\Exception $e) {
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($locale, $id)
	{
		try {
            $estatu = Estatu::find($id);

            if (!empty($estatu)) {

                $this->respuesta["data"] = (object) [
                    "estatu" => __($estatu->estado)
                ];

                return response()->view('estatu.mostrar', $this->respuesta, HttpStatus::OK);
            }
            else {
                $httpStatus = HttpStatus::NOCONTENT;
            }
        } catch (\Exception $e) {
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($locale, $id)
	{
		try {
            $estatu = Estatu::find($id);

            if (!empty($estatu)) {

                $this->respuesta["data"] = (object) [
                    'id' => $estatu->id,
                    'estatu' => $estatu->estado,
                ];

                return response()->view('estatu.editar', $this->respuesta, HttpStatus::OK);
            }
            else {
                $httpStatus = HttpStatus::NOCONTENT;
            }
        } catch (\Exception $e) {
            $this->respuesta["message"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $locale, $id)
	{
		Validator::make($request->all(), [
            'estatu' => ['required', 'regex:/^([a-zA-Z]+(.*))+$/'],
        ])->validate();

        $estatu = Estatu::find($id);
        $estatu->estado = $request->estatu;

        try {
            if ($estatu->isDirty()) {
                $estatu->save();

                /* ========== Register action on bitacora ========== */
                $bitacora = new \App\Bitacora();
                $modulo = \App\Modulo::where('modulo', 'estatus')->first();
                $accion = \App\Accion::where('accion', 'Update')->first();
                $descripcion = "Updated Statu";
                $bitacora->registro($modulo->id, $estatu->id, $accion->id, \Request::ip(), $descripcion);
                /* ================================================= */

                $httpStatus = HttpStatus::OK;
                $this->respuesta["mensaje"] = HttpStatus::OK();
            }
            else {
                $httpStatus = HttpStatus::NOCONTENT;
            }
        } catch (\Exception $e) {
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
            $httpStatus = HttpStatus::ERROR;
        }

        return response()->json($this->respuesta, $httpStatus);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($locale, $id)
	{
		$estatu = Estatu::find($id);

        try {

            $estatu->eliminado = 1;

            $estatu->save();

            /* ========== Register action on bitacora ========== */
            $bitacora = new \App\Bitacora();
            $modulo = \App\Modulo::where('modulo', 'estatus')->first();
            $accion = \App\Accion::where('accion', 'Delete')->first();
            $descripcion = "Deleted Statu";
            $bitacora->registro($modulo->id, $estatu->id, $accion->id, \Request::ip(), $descripcion);
            /* ================================================= */

            $httpStatus = HttpStatus::OK;
            $this->respuesta["mensaje"] = HttpStatus::OK();
        } catch (\Exception $e) {
            $httpStatus = HttpStatus::ERROR;
            $this->respuesta["mensaje"] = HttpStatus::ERROR();
        }

        return response()->json($this->respuesta, $httpStatus);
	}
}
