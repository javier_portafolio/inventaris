<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuloByRol extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modulos_has_roles';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the module
     *
     * @return App\Module
     */
    public function modulo ()
    {
        return $this->belongsTo('App\Modulo', 'modulo_id')->first();
    }

    /**
     * Get the rol
     *
     * @return App\Rol
     */
    public function rol ()
    {
        return $this->belongsTo('App\Rol', 'rol_id')->first();
    }
}
