<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categorias';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the subclase
     *
     * @return App\Subclase
     */
    public function subclase ()
    {
        return $this->belongsTo('App\Subclase', 'sub_clase_id')->first();
    }
}
