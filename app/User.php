<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'cedula',  'genero_id', 'correo', 'estatus_id', 'rol_id', 'username', 'password', 'fecha_registro',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the rol that owns to user
     *
     * @return App\Rol
     */
    public function rol ()
    {
        return $this->belongsTo('App\Rol', 'rol_id')->first();
    }

    /**
     * Get the gender that owns to user
     *
     * @return App\Genero
     */
    public function genero ()
    {
        return $this->belongsTo('App\Genero', 'genero_id')->first();
    }

    /**
     * Get the estatus that owns to user
     *
     * @return App\Estatu
     */
    public function estatu ()
    {
        return $this->belongsTo('App\Estatu', 'estatus_id')->first();
    }
}
