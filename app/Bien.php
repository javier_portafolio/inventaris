<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bien extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bienes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the subcategory
     *
     * @return App\Subcategoria
     */
    public function subcategoria ()
    {
        return $this->belongsTo('App\Subcategoria', 'sub_categoria_id')->first();
    }

    /**
     * Get the mark
     *
     * @return App\Marca
     */
    public function marca ()
    {
        return $this->belongsTo('App\Marca', 'marca_id')->first();
    }

    /**
     * Get the capacity
     *
     * @return App\Capacidad
     */
    public function capacidad ()
    {
        return $this->belongsTo('App\Capacidad', 'capacidad_id')->first();
    }
}
