<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategoria extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sub_categorias';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the category
     *
     * @return App\Categoria
     */
    public function categoria ()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id')->first();
    }
}
