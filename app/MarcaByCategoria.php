<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarcaByCategoria extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'marcas_has_categorias';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the mark
     *
     * @return App\Marca
     */
    public function marca ()
    {
        return $this->belongsTo('App\Marca', 'marca_id')->first();
    }

    /**
     * Get the categoria
     *
     * @return App\Categoria
     */
    public function categoria ()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id')->first();
    }
}
