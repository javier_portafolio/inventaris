<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subclase extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sub_clases';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the clase
     *
     * @return App\Clase
     */
    public function clase ()
    {
        return $this->belongsTo('App\Clase', 'clase_id')->first();
    }
}
