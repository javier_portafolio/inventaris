<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bitacora';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the user that owns the record
     *
     * @return App\User
     */
    public function user ()
    {
        return $this->belongsTo('App\User', 'user_id')->first();
    }

    /**
     * Get the modulo that owns the record
     *
     * @return App\Modulo
     */
    public function modulo ()
    {
        return $this->belongsTo('App\Modulo', 'modulo_id')->first();
    }

    /**
     * Get the accion that owns the record
     *
     * @return App\Accion
     */
    public function accion ()
    {
        return $this->belongsTo('App\Accion', 'accion_id')->first();
    }

    /**
     * Insert data of action
     *
     * @param  int $modulo
     * @param  int $item
     * @param  int    $accion
     * @param  string $ip
     * @param  string $descripcion
     * @return void
     */
    public function registro ($modulo, $item, int $accion, string $ip, string $descripcion)
    {
        $fecha = new \Datetime('now');

        DB::table($this->table)->insert([
            'user_id' => auth()->user()->id,
            'modulo_id' => $modulo,
            'item' => $item,
            'accion_id' => $accion,
            'ip' => $ip,
            'descripcion' => $descripcion,
            'fecha' => $fecha->format('Y-m-d'),
            'hora' => $fecha->format('H:i:s'),
        ]);
    }
}
