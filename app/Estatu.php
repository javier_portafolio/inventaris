<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estatu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estatus';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
