# Inventaris

_Es un sistema de inventario de bienes informaticos, realizado como proyecto de muestra._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

_Tener instalado para poder descargar las dependencias y ejecutar el proyecto_

* [Git](https://git-scm.com/)
* [Composer](https://getcomposer.org/)
* [PHP >= 7.2.5](https://www.php.net/) - Con los modulos
	- BCMath PHP Extension
	- Ctype PHP Extension
	- Fileinfo PHP extension
	- JSON PHP Extension
	- Mbstring PHP Extension
	- OpenSSL PHP Extension
	- PDO PHP Extension
	- Tokenizer PHP Extension
	- XML PHP Extension

_**NOTA:** Puede clonar el proyecto usando los comandos de git o bien descargar alguna [version](https://gitlab.com/javier_portafolio/inventaris/-/releases) en especifico._

### Instalación 🔧

_Teniendo la copia del repositorio en local, es momento de descargar las dependencias para hacerlo funcionar._

_Moverse al directorio que contiene el proyecto y ejecutar `composer install` para descargar las dependencias necesarias del proyecto._

_Realizar una copia del archivo **.env.example** y llamarla **.env**, en este archivo .env estableceremos las credenciales de nuestra base de datos. Para **produccion** las variables **APP_ENV** y **APP_DEBUG** son necesarias, por lo que se deben comentar con un `#` al comienzo de las variables._

_Ejecutamos `php artisan key:generate` para obtener la llave se seguridad de la aplicación._

_Luego `php artisan migrate --seed` para ralizar las migraciones junto con la data necesaria para operar bajo el entorno configurado._

_Y por ultimo ejecutamos `php artisan serve` para levantar nuestro servidor artisan el cual se estara ejecutando en [localhost:8000](http://127.0.0.1:8000)._

_Si todo se configuro adecuada mente la aplicacion ya se estara ejecutando y podra loguear sin problemas._

## Ejecutando las pruebas ⚙️

_Credenciales de ingreso:_

* **Administrador:**
	- Usuario: `admin` Contraseña: `admins`

* **Encargado:**
	- Usuario: `atten` Contraseña: `attens`
	
* **Operador:**
	- Usuario: `operator` Contraseña: `operators`

## Proyecto online
[inventaris](http://inventaris-app.herokuapp.com/en/home)

## Construido con 🛠️

* [Laravel 7.4](https://laravel.com/) - El Framework web usado
* [AdminLTE 3.0](https://adminlte.io/) - Plantilla de administracion
* [PostgreSQL](https://www.postgresql.org/) - Base de Datos

## Autor ✒️

* **[Javier Martinez](https://gitlab.com/can359)** - *Desarrollo y Documentación*
