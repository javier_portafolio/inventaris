<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenerosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generos = [
        	'Female',
        	'Male'
        ];

        foreach ($generos as $value) {
        	DB::table('generos')->insert([
        		'genero' => $value,
        	]);
        }
    }
}
