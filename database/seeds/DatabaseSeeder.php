<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call([
			/* ========== Referential Data ========== */
        	EstatusSeeder::class,
        	GenerosSeeder::class,
        	RolesSeeder::class,
        	ModulosSeeder::class,
        	ModulosHasRolesSeeder::class,
        	AccionesSeeder::class,
        	ClasesSeeder::class,
        	SubclasesSeeder::class,
        	NomenclaturasSeeder::class,
        	MarcasSeeder::class,
        	CapacidadesSeeder::class,
            CategoriasSeeder::class,
            SubcategoriasSeeder::class,
            MarcasHasCategoriasSeeder::class,
        	/* ====================================== */

        	/* ========== Users Data ========== */
        	UsersSeeder::class,
        	/* ================================ */
        ]);
    }
}
