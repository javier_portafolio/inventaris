<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$acciones = (object) [
    		'Create',
    		'Update',
    		'Delete',
    		'Login',
    		'Logout',
    	];

        foreach ($acciones as $value) {
        	DB::table('acciones')->insert([
        		'accion' => $value
        	]);
        }
    }
}
