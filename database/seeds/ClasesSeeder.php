<?php

use Illuminate\Database\Seeder;

class ClasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$clases = [
    		'Computing',
    		'Networking',
    	];

        foreach ($clases as $value) {
        	DB::table('clases')->insert([
        		'clase' => $value
        	]);
        }

    }
}
