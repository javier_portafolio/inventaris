<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estatus = [
        	'Active',
        	'Inactive'
        ];

        foreach ($estatus as $value) {
        	DB::table('estatus')->insert([
        		'estado' => $value,
        	]);
        }
    }
}
