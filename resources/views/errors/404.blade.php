{{-- ========== Base ========== --}}
@extends('layouts.default')
{{-- ========================== --}}

{{-- ========== Tab Title ========== --}}
@section('tab-title', __('404 Page not found'))
{{-- =============================== --}}

{{-- ========== Main CSS ========== --}}
@section('main-css')

	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
	<!-- iCheck -->
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
	<!-- Font: Source Sans Pro -->
	<link href="{{ asset('fonts/SourceSansPro.css') }}" rel="stylesheet">
	<!-- pace-progress -->
	<link rel="stylesheet" href="{{ asset('plugins/pace-progress/themes/blue/pace-theme-center-radar.css') }}">
	<link rel="stylesheet" href="{{ asset('css/home.css') }}">

@endsection
{{-- ============================== --}}

{{-- ========== Main Content ========== --}}
@section('main-content')

	<div class="flex-center position-ref full-height">

        <div class="content">

            <div class="title m-b-md">

            	<i class="fas fa-exclamation-triangle text-danger"></i>
            	<span>400</span>

            </div>

            <div>

            	<p class="text-monospace">{{ __('We could not find the page you were looking for') }}.</p>

            </div>

        	<div>

        		<a href="{{ route('home', ['locale' => app()->getLocale()]) }}" class="btn btn-danger" role="button">{{ __('Go home') }}</a>

	        </div>

        </div>

    </div>

@endsection
{{-- ================================== --}}

{{-- ========== Main JS ========== --}}
@section('main-js')

	<!-- jQuery -->
	<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('js/adminlte.min.js') }}" type="text/javascript"></script>
	<script>
		$('body').addClass('lockscreen')
	</script>
	<!-- pace-progress -->
	<script src="{{ asset('plugins/pace-progress/pace.min.js') }}" type="text/javascript"></script>

@endsection
{{-- ============================= --}}