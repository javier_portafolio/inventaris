{{-- ========== Base ========== --}}
@extends('modals.media')
{{-- ========================== --}}

{{-- ========== Modal Id ========== --}}
@section('modal-id', 'mostrarmarca')
{{-- ============================== --}}

{{-- ========== Modal Title ========== --}}
@section('modal-title')
	{{ __('Show') }} {{ __('Mark') }}
@endsection
{{-- ================================= --}}

{{-- ========== Modal Content ========== --}}
@section('modal-content')

	<table>

		<tbody id="muestraData">

			<tr>
				<th>{{ __('Mark') }}</th>
				<td>{{ $data->marca }}</td>
			</tr>

		</tbody>

	</table>

@endsection
{{-- =================================== --}}

{{-- ========== Modal Footer ========== --}}
@section('modal-footer')

	<button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>

@endsection
{{-- ================================== --}}
