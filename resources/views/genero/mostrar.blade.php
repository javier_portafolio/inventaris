{{-- ========== Base ========== --}}
@extends('modals.media')
{{-- ========================== --}}

{{-- ========== Modal Id ========== --}}
@section('modal-id', 'mostrar')
{{-- ============================== --}}

{{-- ========== Modal Title ========== --}}
@section('modal-title')
	{{ __('Show') }} {{ __('Gender') }}
@endsection
{{-- ================================= --}}

{{-- ========== Modal Content ========== --}}
@section('modal-content')

	<table>

		<tbody id="muestraData">

			<tr>
				<th>{{ __('Gender') }}</th>
				<td>{{ $data->genero }}</td>
			</tr>

		</tbody>

	</table>

@endsection
{{-- =================================== --}}

{{-- ========== Modal Footer ========== --}}
@section('modal-footer')

	<button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>

@endsection
{{-- ================================== --}}
