{{--
|==========================================================================
|  Application     :   Inventaris
|==========================================================================
|  Type            :   template - layouts
|  Description     :   main template
|  Version         :   1.0
|  Laravel version :   7.4.0
|  PHP version     :   7.3.16
|  Purpose         :   Portfolio - Showing
|  Developer       :   Javier Martinez
|  Date            :   Marzo, 2020
|==========================================================================
--}}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

	<meta charset="utf-8">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- favicon -->
	<link rel="icon" href="{{ asset('img/icon70x70.png') }}">

	<!-- Tab Title -->
	<title>{{ config('app.name') }} - @yield('tab-title')</title>

	{{-- ========== Main CSS ========== --}}
	@yield('main-css')
	{{-- ========================= --}}

</head>
<body class="hold-transition pace-primary">

	{{-- ========== Main Content ========== --}}
	@yield('main-content')
	{{-- ================================== --}}

	{{-- ========== Main JS ========== --}}
	@yield('main-js')
	{{-- ======================== --}}

</body>
</html>