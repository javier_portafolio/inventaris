{{-- ========== Base ========== --}}
@extends('layouts.default')
{{-- ========================== --}}

{{-- ========== Main CSS ========== --}}
@section('main-css')

	<!-- pace-progress -->
	<link rel="stylesheet" href="{{ asset('plugins/pace-progress/themes/blue/pace-theme-center-radar.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
	<!-- Font: Source Sans Pro -->
	<link href="{{ asset('fonts/SourceSansPro.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/general.css') }}">
	@yield('css')

@endsection
{{-- ============================== --}}

{{-- ========== Main Content ========== --}}
@section('main-content')

	{{-- ========== Wrapper ========== --}}
	<div class="wrapper">

		{{-- ========== Navbar ========== --}}
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">

			@include('layouts.navbar')

		</nav>
		{{-- ========================= --}}

		{{-- ========== Sidebar ========== --}}
		<aside class="main-sidebar sidebar-dark-primary elevation-4">

			@include('layouts.sidebar')

		</aside>
		{{-- ============================= --}}

		{{-- ========== Content Wrapper ========== --}}
		<div class="content-wrapper">

			{{-- ========== Content Header ========== --}}
			<div class="content-header">

				@include('layouts.content-header')

			</div>
			{{-- ==================================== --}}

			{{-- ========== Content ========== --}}
			<section class="content">

				<div class="container-fluid">

					@yield('content')

				</div>

			</section>
			{{-- ============================= --}}

		</div>
		{{-- ===================================== --}}

		{{-- ========== Footer ========== --}}
		<footer class="main-footer">

			@include('layouts.footer')

		</footer>
		{{-- ============================ --}}

	</div>
	{{-- ============================= --}}

@endsection
{{-- ================================== --}}

{{-- ========== Main JS ========== --}}
@section('main-js')

	<!-- pace-progress -->
	<script data-pace-options='{ "ajax": false }' src="{{ asset('plugins/pace-progress/pace.min.js') }}" type="text/javascript"></script>
	<!-- jQuery -->
	<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button)
	</script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- daterangepicker -->
	<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
	<!-- overlayScrollbars -->
	<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
	<!-- SweetAlert2 -->
	<script src="{{ asset('plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('js/adminlte.min.js') }}" type="text/javascript"></script>
	<!-- General -->
	<script src="{{ asset('js/general.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$('body').addClass('sidebar-mini layout-fixed');

		var data = @json(session()->get('alerta'));

		if (data != null) {
			alertar(data);
		}

	</script>
	@yield('script')

@endsection
{{-- ============================= --}}