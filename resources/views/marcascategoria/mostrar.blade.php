{{-- ========== Base ========== --}}
@extends('modals.media')
{{-- ========================== --}}

{{-- ========== Modal Id ========== --}}
@section('modal-id', 'mostrar')
{{-- ============================== --}}

{{-- ========== Modal Title ========== --}}
@section('modal-title')
	{{ __('Show') }} {{ __('Mark') }} {{ __('by') }} {{ __('Category') }}
@endsection
{{-- ================================= --}}

{{-- ========== Modal Content ========== --}}
@section('modal-content')

	<table>

		<tbody id="muestraData">

			<tr>
				<th>{{ __('Mark') }}</th>
				<td>{{ $data->marca }}</td>
			</tr>

			<tr>
				<th>{{ __('Category') }}</th>
				<td>{{ $data->categoria }}</td>
			</tr>

		</tbody>

	</table>

@endsection
{{-- =================================== --}}

{{-- ========== Modal Footer ========== --}}
@section('modal-footer')

	<button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>

@endsection
{{-- ================================== --}}
