<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Http status Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    '200' => 'Solicitud Exitosa',
    '201' => 'Creado',
    '204' => 'Sin contenido para mostrar',
    '400' => 'Solicitud Incorrecta',
    '403' => 'No tienes los permisos necesarios para acceder',
    '418' => 'Soy una Tetera',
    '500' => '¡Ups! Algo salió mal',

];
