<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Http status Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    '200' => 'Successful Request',
    '201' => 'Created',
    '204' => 'No content to show',
    '400' => 'Bad Request',
    '403' => 'You don\'t have the permissions necessary to access',
    '418' => 'I\'m a Teapot',
    '500' => 'Oops! Something went wrong',

];
