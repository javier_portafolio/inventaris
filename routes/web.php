<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index route without language
Route::get('/', function () {
	return redirect(app()->getLocale() . '/home');
});

// Route that control the language
Route::group([ 'prefix' => '{locale}', 'where' => ['locale' => '[a-z]{2}'], 'middleware' => ['language', 'sidebar'] ], function () {

	// Index route with languague
	Route::get('/home', function () {
		return view('home.index');
	})->name('home');

	// Auth::routes();

	// Auth routes
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	// Call to all web routes
	require "dashboard/DashboardWebRoutes.php";
	require "configuration/ConfigurationWebRoutes.php";
	require "user/UserWebRoutes.php";
	require "bitacora/BitacoraWebRoutes.php";
	require "configuration/ClaseWebRoutes.php";
	require "configuration/SubclaseWebRoutes.php";
	require "configuration/CategoriaWebRoutes.php";
	require "configuration/SubcategoriaWebRoutes.php";
	require "configuration/MarcaWebRoutes.php";
	require "configuration/CapacidadWebRoutes.php";
	require "configuration/EstatuWebRoutes.php";
	require "configuration/GeneroWebRoutes.php";
	require "configuration/ModuloWebRoutes.php";
	require "configuration/NomenclaturaWebRoutes.php";
	require "configuration/RolWebRoutes.php";
	require "configuration/MarcaCategoriaWebRoutes.php";
	require "configuration/ModuloRolWebRoutes.php";
	require "bien/BienWebRoutes.php";

});