<?php

/**
 * --------------------
 * 		Web Routes
 * --------------------
 */

static $uri = 'configurations'; // Control the path uri
static $controller = 'Configuration\ConfigurationController@'; // Control the path to controller

Route::get($uri, $controller . 'index')->name('config.index')->middleware('checkRol:configuraciones,r');