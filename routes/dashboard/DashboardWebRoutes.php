<?php

/**
 * --------------------
 * 		Web Routes
 * --------------------
 */

static $uri = 'dashboard'; // Control the path uri
static $controller = 'Dashboard\DashboardController@'; // Control the path to controller

Route::get($uri, $controller . 'index')->name('dashboard');