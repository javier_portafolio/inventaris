<?php

/**
 * --------------------
 * 		Web Routes
 * --------------------
 */

static $uri = 'binnacle'; // Control the path uri
static $controller = 'Bitacora\BitacoraController@'; // Control the path to controller

Route::get($uri, $controller . 'index')->name('bitacora')->middleware('checkRol:bitacora,r');

Route::get($uri . '/lista', $controller . 'lista')->name('bitacora.list')->middleware('checkRol:bitacora,r');